const path = require('path'),
  webpack = require('webpack'),
  HtmlWebpackPlugin = require('html-webpack-plugin'),
  ExtractTextPlugin = require('extract-text-webpack-plugin'),
  CleanWebpackPlugin = require('clean-webpack-plugin'),
  CopyWebpackPlugin = require('copy-webpack-plugin'),
  ip = require('ip');

const rootPath = path.resolve(__dirname, '../../');
const sourcePath = rootPath + '/frontend';
const buildPath = rootPath + '/';
const ENV = process.env.npm_lifecycle_event;
const isProduction = ENV === 'react:build' ? true : false;
const host = ip.address();
const port = 3001;

const plugins = [
  new HtmlWebpackPlugin({ template: sourcePath + '/index.html' }),
  new ExtractTextPlugin('style/main.[hash].css', { allChunks: true }),
  new CopyWebpackPlugin([{ from: sourcePath + '/media', to: 'media' }]),
];

if (isProduction) {
  plugins.push(
    new CleanWebpackPlugin(['dist'], {
      root: rootPath,
      verbose: true,
      dry: false,
    }),
    new webpack.DefinePlugin({ 'process.env': { NODE_ENV: '"production"' } }),
  );
} else {
  // plugins.push(new webpack.HotModuleReplacementPlugin());
}

const AUTOPREFIXER_BROWSERS = [
  'Android 2.3',
  'Android >= 4',
  'Chrome >= 35',
  'Firefox >= 31',
  'Explorer >= 9',
  'iOS >= 7',
  'Opera >= 12',
  'Safari >= 7.1',
];

module.exports = {
  entry: {
    app: [sourcePath + '/js/index.tsx', sourcePath + '/style/app.scss'],
  },
  output: {
    path: buildPath + '/dist/',
    filename: 'js/[name].[hash].js',
  },
  devtool: 'source-map',
  resolve: {
    extensions: ['.js', '.jsx', '.json', '.ts', '.tsx'],
  },
  module: {
    rules: [
      {
        test: /\.(ts|tsx)$/,
        use: isProduction ? 'ts-loader' : ['babel-loader?plugins=react-hot-loader/babel', 'ts-loader'],
      },
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                url: false,
                minimize: isProduction,
                sourceMap: true,
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                plugins: () => [require('autoprefixer')({ browsers: AUTOPREFIXER_BROWSERS })],
              },
            },
            { loader: 'sass-loader' },
          ],
        }),
      },
    ],
  },
  plugins: plugins,
  devServer: {
    contentBase: sourcePath,
    hot: true,
    inline: true,
    port,
    public: host + ':' + port,
    historyApiFallback: {
      disableDotRule: true,
    },
    stats: 'minimal',
    open: true,
  },
  node: {
    fs: 'empty',
    net: 'empty',
  },
};
