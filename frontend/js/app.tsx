import * as React from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import { Store } from 'redux';
import { Header, Footer } from './components';
import { HelperActions } from './actions';
import { UserModel } from './models';
import { connect } from 'react-redux';
// Components
import Homepage from './views/Homepage';
import EmailSignUp from './views/EmailSignUp';
import Login from './views/Login';
import UserProfile from './views/UserProfile.tsx';
import EditProfile from './views/EditProfile';

interface IProps {
  store: Store;
  user: UserModel;
}
interface IState {}

class App extends React.PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    props.store.dispatch(HelperActions.getDataFromStorage());
    this.state = {};
  }

  render() {
    const { user } = this.props;
    return (
      <Router>
        <div className="app-container">
          <Header user={user} />

          <Route exact path="/" component={Homepage} />
          <Route path="/account/login" component={Login} />
          <Route path="/account/emailsignup" component={EmailSignUp} />
          <Route path="/edit/" component={EditProfile} />
          <Route path="/users/:user/" component={UserProfile} />

          <Footer />
        </div>
      </Router>
    );
  }
}

const mapSateToProps = ({ user }: any) => ({ user: user.user });

export default connect(
  mapSateToProps,
  {},
)(App);
