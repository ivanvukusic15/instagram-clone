import { Store, createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { routerMiddleware } from 'react-router-redux';
import { History } from 'history';
import { logger } from '../middleware';
import { rootReducer } from '../reducers';
import { IRootState } from '../reducers/state';
import thunk from 'redux-thunk';

export function configureStore(history: History, initialState?: IRootState) {
  let middleware = applyMiddleware(logger, routerMiddleware(history), thunk);

  if (process.env.NODE_ENV !== 'production') {
    middleware = composeWithDevTools(middleware);
  }

  const store = createStore(rootReducer as any, initialState as any, middleware) as Store;

  if (module.hot) {
    module.hot.accept('../reducers', () => {
      const nextReducer = require('../reducers');
      store.replaceReducer(nextReducer);
    });
  }

  return store;
}