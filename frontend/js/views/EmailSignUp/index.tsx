import * as React from 'react';
import { connect } from 'react-redux';
import { UserActions } from '../../actions';
import { UserModel } from '../../models';
import { Signup } from '../../components';

interface IProps {
  errors: string | Array<string>;
  history: {
    push: Function,
  };
  loading: boolean;
  user: UserModel;
  userSignup: typeof UserActions.userSignup;
  userUpdate: typeof UserActions.userUpdate;
}
interface IState {}

export class EmailSignup extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <div className="login-container d-flex flex-column align-items-center justify-content-center">
        <Signup
          user={this.props.user}
          loading={this.props.loading}
          push={(value) => this.props.history.push(value)}
          userSignup={(value) => this.props.userSignup(value)}
          userUpdate={(value) => this.props.userUpdate(value)}
        />
      </div>
    );
  }
}

const mapSateToProps = ({ user }: any) => ({ errors: user.errors, user: user.user, loading: user.loading });

export default connect(
  mapSateToProps,
  {
    userSignup: UserActions.userSignup,
    userUpdate: UserActions.userSignup,
  },
)(EmailSignup);
