import * as React from 'react';
import { UserModel } from '../../models';
import { Signup } from '../../components';
import { connect } from 'react-redux';
import { UserActions, HelperActions } from '../../actions';
import { ROOT } from '../../constants';

interface IProps {
  errors: string | Array<string>;
  loading: boolean;
  history: {
    push: Function,
  };
  storageReady: boolean;
  user: UserModel;
  userSignup: typeof UserActions.userSignup;
  userUpdate: typeof UserActions.userUpdate;
  getDataFromStorage: typeof HelperActions.getDataFromStorage;
}
interface IState {}

class Homepage extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {};
  }

  componentDidMount() {
    this.props.getDataFromStorage();
  }

  componentWillReceiveProps(nextProps) {
    const user = nextProps.user;
  }

  renderHomepage() {
    return (
      <div className="homepage-container d-flex flex-row align-items-start justify-content-center">
        <h1>Hello</h1>
      </div>
    );
  }

  renderEmailSignup() {
    return (
      <div className="homepage-container d-flex flex-row align-items-start justify-content-center">
        <img className="home-img d-none d-md-block" src={ROOT + '/media/images/home_mobile.jpg'} />
        <Signup
          errors={this.props.errors}
          loading={this.props.loading}
          user={this.props.user}
          push={(route) => this.props.history.push(route)}
          userSignup={(value) => this.props.userSignup(value)}
          userUpdate={(value) => this.props.userUpdate(value)}
        />
      </div>
    );
  }

  render() {
    return this.props.storageReady && this.props.user && this.props.user.accessToken
      ? this.renderHomepage()
      : this.renderEmailSignup();
  }
}

const mapSateToProps = ({ helper, user }: any) => ({
  errors: user.errors,
  storageReady: helper.storageReady,
  user: user.user,
  loading: user.loading,
});

export default connect(
  mapSateToProps,
  {
    userSignup: UserActions.userSignup,
    userUpdate: UserActions.userUpdate,
    getDataFromStorage: HelperActions.getDataFromStorage,
  },
)(Homepage);
