import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { UserModel } from '../../models';

interface IProps {
  history: {
    push: Function,
  };
  user: UserModel;
}
interface IState extends UserModel {
  item: string;
}

export class UserProfile extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      item: 'edit',
      ...this.props.user,
    };
  }

  render() {
    const { item } = this.state;
    const { username, fullName, website, bio, email, phoneNumber, gender, similarAccountSuggestions } = this.state;
    return (
      <div className="edit-profile-container">
        <div className="inner-content d-flex flex-row align-items-start">
          <ul className="edit-nav">
            <li>
              <a className={item === 'edit' ? 'text-normal bolder selected' : 'text-normal'} href="/edit/">
                Edit Profile
              </a>
            </li>
            <li>
              <a
                className={item === 'change' ? 'text-normal bolder selected' : 'text-normal'}
                href="/accounts/password/change/"
              >
                Change Password
              </a>
            </li>
            <li>
              <a
                className={item === 'manage_access' ? 'text-normal bolder selected' : 'text-normal'}
                href="/accounts/manage_access/"
              >
                Authorized Applications
              </a>
            </li>
            <li>
              <a
                className={item === 'settings' ? 'text-normal bolder selected' : 'text-normal'}
                href="/emails/settings/"
              >
                Email and SMS
              </a>
            </li>
            <li>
              <a
                className={item === 'contact_history' ? 'text-normal bolder selected' : 'text-normal'}
                href="/accounts/contact_history/"
              >
                Manage Contacts
              </a>
            </li>
            <li>
              <a
                className={item === 'privacy_and_security' ? 'text-normal bolder selected' : 'text-normal'}
                href="/accounts/privacy_and_security/"
              >
                Privacy and Security
              </a>
            </li>
          </ul>
          <article className="edit-content">
            <div className="left-side f-flex flex-row align-items-center justify-content-center p-4 mb-3">
              <div className="label-container d-flex flex-row justify-content-end">
                <div className="image" />
              </div>
              <div>
                <h1>nordit.software</h1>
                <a className="button-text">Edit Profile Photo</a>
              </div>
            </div>
            <form className="form" encType="multipart/form-data">
              <div className="left-side f-flex flex-row align-items-start justify-content-center mb-3">
                <div className="label-container d-flex flex-row justify-content-end">
                  <label htmlFor="fullName">Name</label>
                </div>
                <input
                  name="fullName"
                  type="text"
                  value={fullName}
                  onChange={e => this.setState({ fullName: e.target.value })}
                />
              </div>
              <div className="left-side f-flex flex-row align-items-start justify-content-center mb-3">
                <div className="label-container d-flex flex-row justify-content-end">
                  <label htmlFor="username">Username</label>
                </div>
                <input
                  name="username"
                  type="text"
                  value={username}
                  onChange={e => this.setState({ username: e.target.value })}
                />
              </div>
              <div className="left-side f-flex flex-row align-items-start justify-content-center mb-3">
                <div className="label-container d-flex flex-row justify-content-end">
                  <label htmlFor="website">Website</label>
                </div>
                <input
                  name="website"
                  type="text"
                  value={website}
                  onChange={e => this.setState({ website: e.target.value })}
                />
              </div>

              <div className="left-side f-flex flex-row align-items-start justify-content-center mb-3">
                <div className="label-container d-flex flex-row justify-content-end">
                  <label htmlFor="bio">Bio</label>
                </div>
                <textarea name="bio" onChange={e => this.setState({ bio: e.target.value })}>
                  {bio}
                </textarea>
              </div>

              <div className="left-side f-flex flex-row align-items-start justify-content-center mb-1">
                <div className="label-container d-flex flex-row justify-content-end">&nbsp;</div>
                <h2>Private Information</h2>
              </div>
              <div className="left-side f-flex flex-row align-items-start justify-content-center mb-3">
                <div className="label-container d-flex flex-row justify-content-end">
                  <label htmlFor="email">Email</label>
                </div>
                <input
                  name="email"
                  type="text"
                  value={email}
                  onChange={e => this.setState({ email: e.target.value })}
                />
              </div>

              <div className="left-side f-flex flex-row align-items-start justify-content-center mb-3">
                <div className="label-container d-flex flex-row justify-content-end">
                  <label htmlFor="phoneNumber">Phone Number</label>
                </div>
                <input
                  name="phoneNumber"
                  type="text"
                  value={phoneNumber}
                  onChange={e => this.setState({ phoneNumber: e.target.value })}
                />
              </div>

              <div className="left-side f-flex flex-row align-items-start justify-content-center mb-3">
                <div className="label-container d-flex flex-row justify-content-end">
                  <label htmlFor="gender">Gender</label>
                </div>
                <input
                  name="gender"
                  type="text"
                  value={gender}
                  onChange={e => this.setState({ gender: e.target.value })}
                />
              </div>
              <div className="left-side f-flex flex-row align-items-start justify-content-center mb-5">
                <div className="label-container d-flex flex-row justify-content-end">
                  <label htmlFor="username">Similar Account Suggestions</label>
                </div>
                <div className="d-flex flex-row align-items-start justify-content-start">
                  <input
                    type="checkbox"
                    name="similarAccountSuggestions"
                    value="similarAccountSuggestions"
                    onChange={() => this.setState({ similarAccountSuggestions: !similarAccountSuggestions })}
                  />
                  <span className="text-normal text-darker">
                    Include your account when recommending similar accounts people might want to follow.{' '}
                    <a className="button-text mr-3">[?]</a>
                  </span>
                </div>
              </div>

              <div className="left-side f-flex flex-row align-items-start justify-content-center mb-3">
                <div className="label-container d-flex flex-row justify-content-end">&nbsp;</div>
                <div className="d-flex flex-row align-items-center justify-content-between">
                  <input type="submit" name="submit" value="Submit" />
                  <a className="button-text mr-3">Temporary disable my account</a>
                </div>
              </div>
            </form>
          </article>
        </div>
      </div>
    );
  }
}

const mapSateToProps = ({ user }: any) => ({ user: user.user });

export default connect(
  mapSateToProps,
  {},
)(UserProfile);
