import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { UserModel } from '../../models';

interface IProps {
  history: {
    push: Function,
  };
  user: UserModel;
}
interface IState {
  tab: string;
}

export class UserProfile extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      tab: 'posts',
    };
  }

  renderPosts() {
    return (
      <div className="posts">
        <div className="post">
          <div className="inner-container d-flex align-items-center justify-content-center">
            <div className="d-flex flex-row justify-content-center align-items-center">
              <div className="heart-white-full-icon mr-1" />
              <div className="text-large text-white bolder mr-4">12</div>
              <div className="comment-icon mr-1" />
              <div className="text-large text-white bolder">4</div>
            </div>
          </div>
        </div>
        <div className="post">
          <div className="inner-container d-flex align-items-center justify-content-center">
            <div className="d-flex flex-row justify-content-center align-items-center">
              <div className="heart-white-full-icon mr-1" />
              <div className="text-large text-white bolder mr-4">12</div>
              <div className="comment-icon mr-1" />
              <div className="text-large text-white bolder">4</div>
            </div>
          </div>
        </div>
        <div className="post">
          <div className="inner-container d-flex align-items-center justify-content-center">
            <div className="d-flex flex-row justify-content-center align-items-center">
              <div className="heart-white-full-icon mr-1" />
              <div className="text-large text-white bolder mr-4">12</div>
              <div className="comment-icon mr-1" />
              <div className="text-large text-white bolder">4</div>
            </div>
          </div>
        </div>
        <div className="post">
          <div className="inner-container d-flex align-items-center justify-content-center">
            <div className="d-flex flex-row justify-content-center align-items-center">
              <div className="heart-white-full-icon mr-1" />
              <div className="text-large text-white bolder mr-4">12</div>
              <div className="comment-icon mr-1" />
              <div className="text-large text-white bolder">4</div>
            </div>
          </div>
        </div>
      </div>
    );
  }

  renderToDo(tab) {
    return (
      <div className="todo-container">
        <h2>{tab}</h2>
        <h3>TODO</h3>
      </div>
    );
  }

  render() {
    const { tab } = this.state;
    return (
      <div className="user-profile-container">
        <div className="container d-flex flex-column align-items-center justify-content-center">
          <div className="top-container d-flex flex-row align-items-center justify-content-center">
            <div className="image-container">
              <div className="image" />
            </div>
            <div className="user-details-container d-flex flex-column align-items-start justify-content-start">
              <div className="d-flex flex-row align-items-center justify-content-start p-1">
                <h1 className="username-text mr-5">nordit.software</h1>
                <Link className="edit-profile mr-1" to="/account/edit">
                  Edit Profile
                </Link>
                <span className="settings-icon" />
              </div>
              <div className="d-flex flex-row align-items-center justify-content-center p-1">
                <span className="text-normal pr-15">
                  <b>1</b> objava
                </span>
                <span className="text-normal pr-15">
                  Pratitelja: <b>21</b>
                </span>
                <span className="text-normal">
                  Pratim <b>8</b>
                </span>
              </div>
              <div className="text-normal p-1">
                <b>Nordit software</b> Sotware development. Great websites, mobile applications and desktop
                applications.
              </div>
            </div>
          </div>
          <div className="tabs-container d-flex flex-row justify-content-center">
            <div className={tab === 'posts' ? 'tab selected' : 'tab'}>
              <div className={tab === 'posts' ? 'posts-selected-icon icon' : 'posts-icon icon'} />
              <a
                className={tab === 'posts' ? 'text-smaller bolder' : 'text-smaller bolder text-lighter'}
                onClick={() => this.setState({ tab: 'posts' })}
              >
                POSTS
              </a>
            </div>
            <div className={tab === 'igtv' ? 'tab selected' : 'tab'}>
              <a
                className={tab === 'igtv' ? 'text-smaller bolder' : 'text-smaller bolder text-lighter'}
                onClick={() => this.setState({ tab: 'igtv' })}
              >
                IGTV
              </a>
            </div>
            <div className={tab === 'saved' ? 'tab selected' : 'tab'}>
              <div className={tab === 'saved' ? 'saved-selected-icon icon' : 'saved-icon icon'} />
              <a
                className={tab === 'saved' ? 'text-smaller bolder' : 'text-smaller bolder text-lighter'}
                onClick={() => this.setState({ tab: 'saved' })}
              >
                SAVED
              </a>
            </div>
          </div>
          {tab === 'posts' && this.renderPosts()}
          {tab === 'igtv' && this.renderToDo(tab)}
          {tab === 'saved' && this.renderToDo(tab)}
        </div>
      </div>
    );
  }
}

const mapSateToProps = ({ user }: any) => ({ user: user.user });

export default connect(
  mapSateToProps,
  {},
)(UserProfile);
