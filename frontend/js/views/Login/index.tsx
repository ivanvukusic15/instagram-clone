import * as React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { UserActions } from '../../actions';
import { UserModel } from '../../models';
import { InputLogin } from '../../components';
import { ROOT } from '../../constants';

interface IProps {
  errors: Array<string> | string;
  history: {
    push: Function,
  };
  loading: boolean;
  user: UserModel;
  userLogin: typeof UserActions.userLogin;
  userUpdate: typeof UserActions.userUpdate;
}
interface IState {
  email: string;
  emailError: string;
  password: string;
  passwordError: string;
}

export class Login extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      email: '',
      emailError: '',
      password: '',
      passwordError: '',
    };
  }

  componentWillMount() {
    if (this.props.user && this.props.user.accessToken) {
      this.props.history.push('/../');
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user && nextProps.user.accessToken) {
      setTimeout(() => this.props.history.push('/'));
    }
    if (nextProps.errors) {
      this.validateErrors(nextProps.errors);
    }
  }

  sendForm() {
    this.props.userLogin(this.state);
  }

  validateErrors(errors: string | Array<string>) {
    let emailError = '';
    let passwordError = '';
    if (errors && typeof errors === 'string') {
      errors = errors.toLowerCase();
      const errorLower = errors.toLowerCase();
      if (errorLower.includes('email')) {
        emailError = UserActions.Errors[errors];
      }
      if (errorLower.includes('password')) {
        passwordError = UserActions.Errors[errors];
      }
    }
    this.setState({ emailError, passwordError });
  }

  render() {
    const { email, emailError, password, passwordError } = this.state;
    const { errors, loading } = this.props;
    let errorString = '';
    if (errors) {
      if (typeof errors === 'string') {
        errorString = UserActions.Errors[errors];
      } else if (Array.isArray(errors) && errors.length === 1) {
        errorString = UserActions.Errors[errors[0]];
      } else {
        errorString = 'Multiple errors.';
      }
    }
    return (
      <div className="login-container d-flex flex-column align-items-center justify-content-center">
        <section className="login-box d-flex flex-column align-items-center justify-content-center  m-2">
          <img className="login-logo" src={ROOT + '/media/images/instagram_logo.png'} />
          <form className="login-form">
            <InputLogin
              label="Email"
              error={emailError}
              type="email"
              name="email"
              value={email}
              onChange={value => this.setState({ email: value })}
            />
            <InputLogin
              label="Password"
              error={passwordError}
              type="password"
              name="password"
              value={password}
              onChange={value => this.setState({ password: value })}
            />
            <div className="input-container transparent">
              <button
                className={email && password ? 'checked' : ''}
                onClick={e => {
                  e.preventDefault();
                  this.sendForm();
                }}
              >
                {loading ? '...' : 'Login'}
              </button>
            </div>
          </form>
          {errors && (
            <div className="w-100 m-1">
              <p className="text-red text-small">{errorString}</p>
            </div>
          )}
          <div className="p-3">
            <Link className="link-dark-blue text-smaller" to="/">
              Forgot your password?
            </Link>
          </div>
        </section>
        <section className="login-box text-small p-4  m-2">
          <div className="text-darker">
            Don't have an account?
            <Link className="link-light-blue" to="/emailsignup">
              {} Sign up
            </Link>
          </div>
        </section>
        <div className="text-small text-darker">
          <p>Get the app</p>
        </div>
        <div className="d-flex flex-row align-items-center justify-content-between m-2">
          <img className="store-img" src={ROOT + '/media/images/download_on_app_store.png'} />
          <img className="store-img" src={ROOT + '/media/images/download_on_google_play.png'} />
        </div>
      </div>
    );
  }
}

const mapSateToProps = ({ user }: any) => ({ errors: user.errors, user: user.user, loading: user.loading });

export default connect(
  mapSateToProps,
  {
    userLogin: UserActions.userLogin,
    userUpdate: UserActions.userUpdate,
  },
)(Login);
