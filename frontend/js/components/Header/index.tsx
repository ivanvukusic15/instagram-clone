import * as React from 'react';
import { UserModel } from '../../models';
import { Link } from 'react-router-dom';

interface IProps {
  user: UserModel;
}
interface IState {
  search: string;
  searchFocused: boolean;
}

export class Header extends React.PureComponent<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      search: '',
      searchFocused: false,
    };
  }

  public render(): JSX.Element {
    const { search, searchFocused } = this.state;
    const { accessToken, username } =  this.props.user;
    if (accessToken) {
      return (
        <header className="header d-flex flex-column align-items-center justify-content-center">
          <div className="container d-flex flex-row align-items-center justify-content-between">
            <div className="d-flex flex-row align-items-center">
              <img className="header-icon" src="./media/images/instagram_icon.png" />
              <span className="divider" />
              <img className="header-logo" src="./media/images/instagram_logo.png" />
            </div>
            <div className="d-flex flex-row align-items-center">
              <div className={searchFocused ? "search-container selected" : "search-container"}>
                <input className="text-input" type="text" placeholder="Search" value={search}
                  onBlur={() => setTimeout(() => this.setState({searchFocused: false }))}
                  onFocus={() => this.setState({searchFocused: true })}
                  onChange={(e) => this.setState({search: e.target.value })}
                />
                <div className="search-icon" />
                {search && searchFocused && (
                  <div className="delete-icon-container"
                    onClick={(e) => this.setState({search: ''})}>
                    <div className="delete-icon" />
                  </div>
                  )}
              </div>
            </div>
            <div className="header-icons d-flex flex-row align-items-center">
              <Link to="/" className="explore-icon" />
              <span className="heart-icon" />
              <Link to={`/users/${username}`} className="profile-icon" />
            </div>
          </div>
        </header>
      );
    }
    return null;
  }
}
