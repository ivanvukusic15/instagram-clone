import * as React from 'react';
import { Link } from 'react-router-dom';

interface IProps {}

export class Footer extends React.PureComponent<IProps> {
  constructor(props: IProps) {
    super(props);
  }

  render(): JSX.Element {
    return (
      <footer className="footer container d-flex flex-row justify-content-between align-items-center">
        <ul className="footer-menu d-flex flex-row">
          <li>
            <Link className="link-dark-blue bold text-smaller" to="/">
              ABOUT
            </Link>
          </li>
          <li>
            <Link className="link-dark-blue bold text-smaller" to="/">
              LANGUAGE
            </Link>
          </li>
          <li>
            <Link className="link-dark-blue bold text-smaller" to="/">
              TECHNOLOGIES
            </Link>
          </li>
        </ul>
        <div className="footer-copywright bold text-small">©2018 INSTAGRAM</div>
      </footer>
    );
  }
}
