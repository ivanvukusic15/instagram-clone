import * as React from 'react';
import { Link } from 'react-router-dom';
import { UserModel } from '../../models';
import { InputLogin } from '../../components';
import FaFacebook from 'react-icons/lib/fa/facebook';
import { UserActions } from '../../actions';
import { USER_INITIAL_STATE } from '../../reducers/UserReducer';

interface IProps {
  errors?: Array<string> | string;
  loading: boolean;
  push: Function;
  user: UserModel;
  userSignup: Function;
  userUpdate: typeof UserActions.userUpdate;
}
interface IState {
  email: string;
  emailError: string;
  errors: Array<string> | string;
  fullName: string;
  fullNameError: string;
  password: string;
  passwordError: string;
  username: string;
  usernameError: string;
  message: string;
}

export class Signup extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      email: '',
      emailError: '',
      errors: null,
      fullName: '',
      fullNameError: '',
      password: '',
      passwordError: '',
      username: '',
      usernameError: '',
      message: '',
    };
  }

  componentWillMount() {
    if (this.props.user && this.props.user.accessToken) {
      this.props.push('/');
    }
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.user._id && !nextProps.errors) {
      this.setState(
        {
          message: 'User successfully created.',
          email: '',
          emailError: '',
          errors: null,
          fullName: '',
          fullNameError: '',
          password: '',
          passwordError: '',
          username: '',
          usernameError: '',
        },
        () => {
          setTimeout(() => {
            this.props.push('/login');
            this.props.userUpdate(USER_INITIAL_STATE);
          }, 2000);
        },
      );
    }
    if (nextProps.errors) {
      this.validateErrors(nextProps.errors);
    }
  }

  sendForm() {
    this.props.userUpdate({ user: this.props.user, errors: this.props.errors, loading: true });
    setTimeout(() => this.props.userSignup(this.state));
  }

  validateErrors(errors: string | Array<string>) {
    let emailError = '';
    let fullNameError = '';
    let passwordError = '';
    let usernameError = '';
    if (typeof errors === 'string') {
      errors = errors.toLowerCase();
      const errorLower = errors.toLowerCase();
      if (errorLower.includes('email')) {
        emailError = UserActions.Errors[errors];
      }
      if (errorLower.includes('fullname')) {
        fullNameError = UserActions.Errors[errors];
      }
      if (errorLower.includes('password')) {
        passwordError = UserActions.Errors[errors];
      }
      if (errorLower.includes('username')) {
        fullNameError = UserActions.Errors[errors];
      }
    } else {
      errors.map(error => {
        error.toLowerCase();
        const errorLower = error.toLowerCase();
        if (errorLower.includes('email')) {
          emailError = UserActions.Errors[error];
        }
        if (errorLower.includes('fullname')) {
          fullNameError = UserActions.Errors[error];
        }
        if (errorLower.includes('password')) {
          passwordError = UserActions.Errors[error];
        }
        if (errorLower.includes('username')) {
          usernameError = UserActions.Errors[error];
        }
      });
    }
    this.setState({ emailError, fullNameError, passwordError, usernameError });
  }

  render() {
    const {
      email,
      emailError,
      fullName,
      fullNameError,
      message,
      password,
      passwordError,
      username,
      usernameError,
    } = this.state;
    const { errors, loading } = this.props;
    let errorString = '';
    if (errors) {
      if (typeof errors === 'string') {
        errorString = UserActions.Errors[errors];
      } else if (Array.isArray(errors) && errors.length === 1) {
        errorString = UserActions.Errors[errors[0]];
      } else if (Array.isArray(errors)) {
        errorString = 'Multiple errors.';
      }
    }
    return (
      <div className="login-container d-flex flex-column align-items-center justify-content-center">
        <section className="login-box d-flex flex-column align-items-center justify-content-center  m-2">
          <img className="login-logo" src="./media/images/instagram_logo.png" />
          <div className="signup-text-container">
            <h5 className="bold">Sign up to see photos and videos from your friends.</h5>
          </div>
          <form className="login-form">
            <div className="input-container transparent">
              <button
                className={'checked'}
                onClick={e => {
                  e.preventDefault();
                  this.sendForm();
                }}
              >
                <FaFacebook />
                Log in with Facebook
              </button>
            </div>
            <div className="divider d-flex flex-row align-items-center justify-content-center p-3">
              <div className="line" />
              <div className="text">or</div>
              <div className="line" />
            </div>
            <InputLogin
              label="Email"
              error={emailError}
              type="email"
              name="email"
              value={email}
              onChange={value => this.setState({ email: value })}
            />
            <InputLogin
              label="Full name"
              error={fullNameError}
              type="text"
              name="fullName"
              value={fullName}
              onChange={value => this.setState({ fullName: value })}
            />
            <InputLogin
              label="Username"
              error={usernameError}
              type="text"
              name="username"
              value={username}
              onChange={value => this.setState({ username: value })}
            />
            <InputLogin
              label="Password"
              error={passwordError}
              type="password"
              name="password"
              value={password}
              onChange={value => this.setState({ password: value })}
            />
            <div className="input-container transparent">
              <button
                className="checked"
                onClick={e => {
                  e.preventDefault();
                  this.sendForm();
                }}
              >
                {loading ? '...' : 'Next'}
              </button>
            </div>
            {errors && (
              <div className="w-100 m-1">
                <p className="text-red text-small">{errorString}</p>
              </div>
            )}
            {message && (
              <div className="w-100 m-1">
                <p className="text-green text-small">{message}</p>
              </div>
            )}
            <div className="signup-text-container">
              <p>
                By signing up, you agree to our <Link to="/">Terms</Link>. Learn how we collect, use and share your data
                in our <Link to="/">Data Policy </Link>
                and how we use cookies and similar technology in our <Link to="/">Cookies Policy</Link>.
              </p>
            </div>
          </form>
        </section>
        <section className="login-box text-normal p-4  m-2">
          <div className="text-darker">
            Have an account?
            <Link className="link-light-blue" to="/login">
              {' '}
              Log in
            </Link>
          </div>
        </section>
        <div className="text-small text-darker">
          <p>Get the app</p>
        </div>
        <div className="d-flex flex-row align-items-center justify-content-between m-2">
          <img className="store-img" src="./media/images/download_on_app_store.png" />
          <img className="store-img" src="./media/images/download_on_google_play.png" />
        </div>
      </div>
    );
  }
}
