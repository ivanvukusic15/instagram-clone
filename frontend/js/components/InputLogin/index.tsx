import * as React from 'react';
import MdClose from 'react-icons/lib/md/close';

interface IProps {
  error?: string;
  label: string;
  name: string;
  type: string;
  value?: string;
  onChange: Function;
}
interface IState {
  focused: Boolean;
}

export class InputLogin extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);
    this.state = {
      focused: false,
    };
  }

  render() {
    const { focused } = this.state;
    const { error, label, name, type, value, onChange } = this.props;
    let containerClassName = 'input-container ';
    if (value) {
      containerClassName += 'not-empty ';
    }
    if (error) {
      containerClassName += 'error ';
    }
    if (focused) {
      containerClassName += 'focused ';
    }
    return (
      <div className={containerClassName}>
        <label htmlFor={name}>{label}</label>
        <input
          type={type}
          name={name}
          value={value}
          onFocus={() => this.setState({ focused: true })}
          onBlur={() => this.setState({ focused: false })}
          onChange={(e: any) => onChange(e.target.value)}
        />
        {error && (
          <div className="error-x-container d-flex align-items-center justify-content-center">
            <MdClose />
          </div>
        )}
      </div>
    );
  }
}
