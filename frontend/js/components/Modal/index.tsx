import * as React from 'react';
import { Link } from 'react-router-dom';

interface IProps {}

export class Modal extends React.PureComponent<IProps> {
  constructor(props: IProps) {
    super(props);
  }

  render(): JSX.Element {
    return (
      <div className="modal">
        <div className="inner-container">
          <Link className="" to="#">
            Uplodad Photo
          </Link>
          <Link className="" to="#">
            Remove Current Photo
          </Link>
          <Link className="" to="#">
            Cancel
          </Link>
        </div>
      </div>
    );
  }
}
