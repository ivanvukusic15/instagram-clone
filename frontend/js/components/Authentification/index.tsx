import * as React from 'react';
import { connect } from 'react-redux';

interface IProps {
  accessToken: string;
  history: {
    push: Function,
  };
}

export const Authentification = (ComposedComponent: any) => {
  class Authentication extends React.PureComponent<IProps> {
    constructor(props: IProps) {
      super(props);
    }

    componentWillMount() {
      if (!this.props.accessToken) {
        this.props.history.push('/login');
      }
    }

    componentWillUpdate(nextProps: IProps) {
      if (!nextProps.accessToken) {
        this.props.history.push('/login');
      }
    }

    render() {
      return <ComposedComponent {...this.props} />;
    }
  }

  function mapStateToProps({ user }: any) {
    return { accessToken: user.user.accessToken };
  }

  return connect(mapStateToProps)(Authentication);
};
