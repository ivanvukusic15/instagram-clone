export const IP = '192.168.1.176';
export const ROOT = 'http://' + IP + ':3001/';
export const API_ROOT = 'http://' + IP + ':3000/';
export const API = API_ROOT + 'api/v1/';

export const URLS = {
  user: {
    login: API + 'users/login',
    signup: API + 'users/register',
  },
};
