import { createActionThunk } from 'redux-thunk-actions';
import { createAction } from 'redux-actions';
import { URLS } from '../../constants';
import axios from 'axios';
import { updateStorage } from '../HelperActions';
import { UserReducer } from '../../models';

const userLoginFunction = async ({ email, password }) => {
  const url = URLS.user.login;
  try {
    const response = await axios.post(url, { email, password });
    if (response.data.success) { 
      return { user: response.data.data };
    } else {
      return { errors: response.data.message };
    }
  } catch(errors) {
    return { errors: 'error' };
  }
};

const userSignupFunction = async ({ email, fullName, password, username }) => {
  const url = URLS.user.signup;
  try {
    const response = await axios.post(url, { email, fullName, password, username });
    if (response.data.success) { 
      return { user: response.data.data };
    } else {
      return { errors: response.data.message  };
    }
  } catch(error) {
    return { errors: 'error' };
  }
};

// EXPORT ACTIONS
export namespace UserActions {
  export enum Type {
    USER_UPDATE = 'user_update',
    USER_LOGIN = 'user_login',
    USER_LOGIN_SUCCEEDED = 'user_login_SUCCEEDED',
    USER_SIGNUP = 'user_signup',
    USER_SIGNUP_SUCCEEDED = 'user_signup_SUCCEEDED',
  }

  export const Errors = {
    alreadyExists: 'User already exists.',
    emptyUser: 'Wrong username, email or password.',
    error: 'Something went wrong.',
    fullNameEmpty: 'Full name cannot be empty.',
    invalidEmailAdrress: 'Invalid email address.',
    passwordEmpty: 'Password empty or too short.',
    serverError: 'Something went wrong.',
    usernameEmpty: 'Username cannot be empty.',
    wrongUsernamePassword: 'Wrong username, email or password.',
  };

  export const userLogin = createActionThunk(
    Type.USER_LOGIN,
    async ({ email, password }) => {
      const data = await userLoginFunction({ email, password });
      if (!data.errors) {
        await updateStorage({ data: data.user, type: 'user' });
      }
      return data;
    },
  );
  export const userSignup = createActionThunk(
    Type.USER_SIGNUP,
    async ({ email, fullName, password, username }) => {
      const data = await userSignupFunction({ email, fullName, password, username });
      return data;
    },
  );
  export const userUpdate = createAction<UserReducer>(Type.USER_UPDATE);
}
