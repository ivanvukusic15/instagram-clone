import { createActionThunk } from 'redux-thunk-actions';

export const getDataFromStorageFunction = async () => {
  // localStorage.removeItem('instagramClone');
  const storage: string = await localStorage.getItem('instagramClone');
  if (storage) {
    return JSON.parse(storage);
  }
  return null;
};

export const updateStorage = async ({type, data}) => {
  const storage: string = await localStorage.getItem('instagramClone');
  if (storage) {
    const newStorage = { ...JSON.parse(storage), [type]: data };
    localStorage.setItem('instagramClone', JSON.stringify(newStorage));
  } else {
    const newStorage = { [type]: data };
    localStorage.setItem('instagramClone', JSON.stringify(newStorage));
  }
};

// EXPORT ACTIONS
export namespace HelperActions {
  export enum Type {
    HELPER_GET_DATA_FROM_STORAGE = 'helper_get_data_from_storage',
    HELPER_GET_DATA_FROM_STORAGE_SUCCEEDED = 'helper_get_data_from_storage_SUCCEEDED',
  }

  export const getDataFromStorage: any = createActionThunk(HelperActions.Type.HELPER_GET_DATA_FROM_STORAGE,
    async () => {
      const data = await getDataFromStorageFunction();
      return data;
    },
  );
}
