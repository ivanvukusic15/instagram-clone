import { handleActions } from 'redux-actions';
import { HelperActions } from '../../actions';

export const HELPER_INITIAL_STATE = {
  storageReady: false,
};

export const HelperReducer = handleActions(
  {
    [HelperActions.Type.HELPER_GET_DATA_FROM_STORAGE_SUCCEEDED]: (state, action) => {
      return { ...state, storageReady: true };
    },
  },
  HELPER_INITIAL_STATE,
);
