import { combineReducers } from 'redux';
import { IRootState } from './state';
import { routerReducer, RouterState } from 'react-router-redux';
import { UserReducer } from './UserReducer';
import { HelperReducer } from './HelperReducer';

// export { IRootState, RouterState };

export const rootReducer = combineReducers<IRootState>({
  helper: HelperReducer as any,
  user: UserReducer as any,
  router: routerReducer as any,
});
