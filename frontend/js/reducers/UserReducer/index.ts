import { handleActions } from 'redux-actions';
import { HelperActions, UserActions } from '../../actions';

export const USER_INITIAL_STATE = {
  user: {
    _id: '',
    accessToken: '',
    fullName: '',
    username: '',
    email: '',
    password: '',
    website: '',
    bio: '',
    phoneNumber: '',
    gender: '',
    similarAccountSuggestions: false,
  },
  errors: null,
  loading: false,
};

// export const UserReducer = (state = INITIAL_STATE, action) => {
//   console.log(action);
//   return state;
// };

export const UserReducer = handleActions(
  {
    [UserActions.Type.USER_UPDATE]: (state, action) => {
      if (action.payload) {
        return { ...state, ...action.payload };
      } else {
        return { ...state, loading: false };
      }
    },
    [HelperActions.Type.HELPER_GET_DATA_FROM_STORAGE_SUCCEEDED]: (state, action) => {
      if (action.payload && action.payload.user) {
        return { ...state, ...action.payload, errors: null, loading: false };
      } else if (action.payload && action.payload.errors) {
        return { ...state, ...action.payload, loading: false };
      } else {
        return { ...state, loading: false };
      }
    },
    [UserActions.Type.USER_LOGIN_SUCCEEDED]: (state, action) => {
      if (action.payload && action.payload.user) {
        return { ...state, ...action.payload, errors: null, loading: false };
      } else if (action.payload && action.payload.errors) {
        return { ...state, ...action.payload, loading: false };
      } else {
        return { ...state, loading: false };
      }
    },
    [UserActions.Type.USER_SIGNUP_SUCCEEDED]: (state, action) => {
      if (action.payload && action.payload.user) {
        return { ...state, ...action.payload, errors: null, loading: false };
      } else if (action.payload.errors) {
        return { ...state, ...action.payload, user: USER_INITIAL_STATE.user, loading: false };
      } else {
        return { ...state, loading: false };
      }
    },
  },
  USER_INITIAL_STATE,
);
