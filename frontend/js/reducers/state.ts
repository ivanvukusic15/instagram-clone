import { HelperReducer, UserReducer } from '../models';
import { RouterState } from 'react-router-redux';

export interface IRootState {
  helper: RootState.HelperState;
  user: RootState.UserState;
  router: RouterState;
}

export namespace RootState {
  export type UserState = UserReducer[];
  export type HelperState = HelperReducer[];
}
