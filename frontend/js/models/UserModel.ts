export interface UserModel {
  _id: string;
  accessToken: string;
  fullName: string;
  username: string;
  email: string;
  password: string;
  website: string;
  bio: string;
  phoneNumber: string;
  gender: string;
  similarAccountSuggestions: boolean;
}

export interface UserReducer {
  user: UserModel;
  errors: string | Array<string>;
  loading: boolean;
}
