import { Document, Schema, Model, model, Error } from 'mongoose';
import * as bcrypt from 'bcrypt';

export interface IUserModel extends Document {
  _id: string;
  accessToken: string;
  email: string;
  fullName: string;
  username: string;
  password: string;
  comparePassword: comparePasswordFunction;
  createUser: createUserFunction;
}

type createUserFunction = (newUser: IUserModel, callback: Object) => void;
type comparePasswordFunction = (candidateHash: String, hash: String, callback: Object) => void;

export const UserSchema: Schema = new Schema({
  createdAt: { type: Date, default: Date.now() },
  updatedAt: { type: Date, default: Date.now() },
  email: { type: String, default: '', unique: true, lowercase: true },
  fullName: String,
  password: { type: String, default: '' },
  posts: [{ type: Schema.Types.ObjectId, ref: 'Post' }],
  username: { type: String, default: '', unique: true, lowercase: true },
  isDeleted: { type: Boolean, default: false },
});

const createUser: createUserFunction = (newUser, callback) => {
  bcrypt.genSalt(10, function(err, salt) {
    bcrypt.hash(newUser.password, salt, function(err, hash) {
      newUser.password = hash;
      newUser.save(callback);
    });
  });
};
UserSchema.methods.createUser = createUser;

const comparePassword: comparePasswordFunction = (candidatePassword, hash, callback) => {
  bcrypt.compare(candidatePassword, hash, callback);
};
UserSchema.methods.comparePassword = comparePassword;

export const User: Model<IUserModel> = model < IUserModel > ('User', UserSchema);
