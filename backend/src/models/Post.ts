import { Schema, model } from 'mongoose';

let PostSchema: Schema = new Schema({
  createdAt: { type: Date, default: Date.now() },
  updatedAt: { type: Date, default: Date.now() },
  title: { type: String, defautl: '' },
  slug: { type: String, defautl: '', unique: true, lowercase: true },
  content: { type: String, defautl: '' },
  featuredImage: { type: String, default: '' },
  category: { type: [String], default: '' },
  published: { type: Boolean, default: true },
  isDeleted: { type: Boolean, default: false },
  _creator: { type: Schema.Types.ObjectId, ref: 'User' },
});

const checkField = function(next) {
  if (!this.title) {
    const err = new Error('Title cannot be blank.');
    next(err);
  }
  if (!this.content) {
    const err = new Error('Content cannot be blank.');
    next(err);
  }
  if (!this.featuredImage) {
    const err = new Error('Featured image cannot be blank.');
    next(err);
  }
  if (!this.category || this.category.length === 0) {
    const err = new Error('Category cannot be blank.');
    next(err);
  }
  next();
};
PostSchema.pre('save', checkField);

export default model('Post', PostSchema);
