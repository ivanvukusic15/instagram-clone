import { Schema, Document, Model, model } from 'mongoose';

export interface IUserSessionModel extends Document {
  _user: string;
}

let UserSessionSchema: Schema = new Schema({
  accessToken: { type: String, default: '' },
  createdAt: { type: Date, default: Date.now() },
  updatedAt: { type: Date, default: Date.now() },
  isDeleted: { type: Boolean, default: false },
  _userId: { type: Schema.Types.ObjectId, ref: 'User' },
});

export const UserSession: Model<IUserSessionModel> = model < IUserSessionModel > ('UserSession', UserSessionSchema);
