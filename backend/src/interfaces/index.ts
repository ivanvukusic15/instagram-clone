import { Request } from 'express';

export interface ICustomRequest extends Request {}

export interface ISession {
  app: String;
  user: String;
}

export interface IError {
  location: String;
  msg: String;
}
