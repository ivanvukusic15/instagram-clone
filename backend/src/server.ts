import * as express from 'express';
import * as mongoose from 'mongoose';
import * as bodyParser from 'body-parser';
import * as path from 'path';
import * as logger from 'morgan';
import * as helmet from 'helmet';
import * as compression from 'compression';
import * as cors from 'cors';

// import controllers
import IndexController from './controllers/IndexController';
import UserController from './controllers/UserController';
import PostController from './controllers/PostController';

// server class
class Server {
  public app: express.Application;

  constructor() {
    this.app = express();
    this.config();
    this.routes();
  }

  public config() {
    // Set up Mongoose
    const MONGO_URI: string = 'mongodb://localhost/instagram_clone';
    mongoose.connect(MONGO_URI || process.env.MONGO_URI);

    // Body Parser
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(bodyParser.json());

    // Various config
    this.app.use(helmet());
    this.app.use(logger('dev'));
    this.app.use(compression());

    // CORS
    this.app.use(cors());
    this.app.use((req, res, next) => {
      res.header('Access-Control-Allow-Origin', '*');
      res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
      res.header(
        'Access-Control-Allow-Headers',
        'Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials',
      );
      res.header('Access-Control-Allow-Credentials', 'true');
      next();
    });

    //View engine
    this.app.set('views', path.join(__dirname, '../src/views'));
    this.app.set('view engine', 'ejs');

    // Set Static Folder
    this.app.use(express.static(path.join(__dirname, '../dist')));
  }

  public routes() {
    let router: express.Router;
    router = express.Router();

    this.app.use('/', IndexController);
    this.app.use('/api/v1/posts', PostController);
    this.app.use('/api/v1/users', UserController);
  }
}

export default new Server().app;
