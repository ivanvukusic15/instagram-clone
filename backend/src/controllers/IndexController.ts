import { Router, Request, Response } from 'express';

export class IndexController {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  public renderFrontend(req: Request, res: Response): void {
    res.redirect('/');
  }

  routes() {
    // this.router.get('/login', this.renderLogin);
    this.router.get('*/*', this.renderFrontend);
  }
}

const indexController = new IndexController();

export default indexController.router;
