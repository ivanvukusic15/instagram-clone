import { Router, Request, Response, NextFunction } from 'express';
import Post from '../models/Post';
import slugify from 'slugify';

export class PostController {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  public GetPosts(req: Request, res: Response): void {
    Post.find({})
      .then((data) => {
        res.status(res.statusCode).json({
          success: true,
          data,
        });
      })
      .catch((err) => {
        res.status(res.statusCode).json({
          success: false,
          error: err.toString().replace('Error: ', ''),
        });
      });
  }

  public GetPost(req: Request, res: Response): void {
    const slug: string = req.params.slug;

    Post.findOne({ slug })
      .then((data) => {
        res.status(res.statusCode).json({
          success: true,
          data,
        });
      })
      .catch((err) => {
        res.status(res.statusCode).json({
          success: false,
          error: err.toString().replace('Error: ', ''),
        });
      });
  }

  public CreatePost(req: Request, res: Response): void {
    const title: string = req.body.title;
    const featuredImage: string = req.body.featuredImage;
    const content: string = req.body.content;
    const slug: string = slugify(title, { replacement: '-', remove: null, lower: true });
    const category: Array<string> = req.body.category;
    const post = new Post({ title, featuredImage, content, slug, category });
    post
      .save()
      .then((data) => {
        res.status(res.statusCode).json({ success: true, data });
      })
      .catch((err) => {
        res.status(res.statusCode).json({ success: true, error: err.toString().replace('Error: ', '') });
      });
  }

  public UpdatePost(req: Request, res: Response): void {
    const slug: string = req.params.slug;

    Post.findOneAndUpdate({ slug }, req.body)
      .then((data) => {
        res.status(res.statusCode).json({
          success: true,
          data,
        });
      })
      .catch((err) => {
        res.status(res.statusCode).json({
          success: true,
          error: err.toString(),
        });
      });
  }

  public DeletePost(req: Request, res: Response): void {
    const slug: string = req.params.slug;

    Post.findOneAndRemove({ slug })
      .then((data) => {
        res.status(res.statusCode).json({
          success: true,
          data,
        });
      })
      .catch((err) => {
        res.status(res.statusCode).json({
          success: true,
          error: err.toString(),
        });
      });
  }

  routes() {
    this.router.get('/', this.GetPosts);
    this.router.get('/:slug', this.GetPost);
    this.router.post('/', this.CreatePost);
    this.router.put('/:slug', this.UpdatePost);
    this.router.delete('/:slug', this.DeletePost);
  }
}

const postController = new PostController();

export default postController.router;
