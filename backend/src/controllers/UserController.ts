import { Router, Request, Response, NextFunction } from 'express';
import { User, IUserModel } from '../models/User';
import { UserSession } from '../models/UserSession';
import { body, validationResult } from 'express-validator/check';
import * as jwt from 'jsonwebtoken';


export class UserController {
  router: Router;

  constructor() {
    this.router = Router();
    this.routes();
  }

  public getUsers(req: Request, res: Response): void {
    User.find({})
      .then((data) => {
        res.status(res.statusCode).json({
          success: true,
          data,
        });
      })
      .catch((err) => {
        res.status(res.statusCode).json({
          success: false,
          message: err.toString().replace('Error: ', ''),
        });
      });
  }

  public getUser(req: Request, res: Response): void {
    const email: string = req.params.email;
    User.findOne({ email })
      .then((data) => {
        if (data && Object.keys(data).length > 0) {
          res.status(res.statusCode).json({
            success: true,
            data,
          });
        } else {
          res.status(res.statusCode).json({ success: false, message: 'emptyUser' });
        }
      })
      .catch((err) => {
        res.status(res.statusCode).json({
          success: false,
          message: err.toString().replace('Error: ', ''),
        });
      });
  }

  public registerUser(req: Request, res: Response): void {
    let email: string = req.body.email;
    email && email.toLowerCase();
    const fullName: string = req.body.fullName;
    const username: string = req.body.username;
    const password: string = req.body.password;
    const user = { fullName, username, email, password };

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const returnErrors = [];
      errors.array().map(error => returnErrors.push(error['msg']))
      res.status(res.statusCode).json({
        success: false,
        message: returnErrors,
      });
    } else {
      const newUser = new User(user);
      newUser.createUser(newUser, (error, user) => {
        if (error) {
          if (error.code) {
            res.status(res.statusCode).json({
              success: false,
              message: 'alreadyExists',
            });
          } else {
            res.status(res.statusCode).json({
              success: false,
              message: 'serverError',
            });
          }
        } else {
          const data = {
            _id: user._id,
            email: user.email,
            createdAt: user.createdAt,
            username: user.username,
          }
          res.status(res.statusCode).json({
            success: true,
            data,
          });
        }
      })
    } 
  }

  public loginUser(req: Request, res: Response): void {
    const email: string = req.body.email;
    const password: string = req.body.password;
    const query = User.findOne({ email: email });
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
      const errorsCount = errors.array().length;
      const returnErrors = [];
      errors.array().map(error => returnErrors.push(error['msg']))
      res.status(res.statusCode).json({
        success: false,
        message: returnErrors,
      });
    } else {
      query.findOne({ email }, (err, user: IUserModel) => {
        if (err) {
          res.status(res.statusCode).json({
            success: false,
            message: 'serverError',
          });
        } else if (user) {
          user.comparePassword(password, user.password, (err, isMatch) => {
            if (err) {
              res.status(res.statusCode).json({
                success: false,
                message: 'wrongUsernamePassword',
              });
            } else if (!isMatch) {
              res.status(res.statusCode).json({
                success: false,
                message: 'wrongUsernamePassword',
              });
            } else {
              let data: any = { 
                _id: user['_doc']._id, 
                fullName: user['_doc'].fullName, 
                email,
                username: user['_doc'].username, 
              };
              const newUserSession = new UserSession({ _user: data });
              newUserSession.save((error, userSession) => {
                if(error){
                  res.status(res.statusCode).json({
                    success: false,
                    message: 'serverError',
                  });
                } else {
                  res.status(res.statusCode).json({
                    success: true,
                    data: {
                      ...data, accessToken: userSession['_doc']._id,
                    },
                  });
                }
              });
            }
          });
        } else {
          res.status(res.statusCode).json({
            success: false,
            message: 'emptyUser',
          });
        }      
      });
    }
  }

  public verifyToken(req: Request, res: Response): void {
    const token: string = req.body.accessToken;
    jwt.verify(token, 'instagram_clone', (err, decoded) => {
      if (err) {
        res.status(res.statusCode).json({
          success: false,
          message: err
        });
      }
      res.status(res.statusCode).json({
        success: true,
        data: decoded.user
      });
    });
  }

  public updateUser(req: Request, res: Response): void {
    const email: string = req.params.email;
    User.findOneAndUpdate({ email }, { $set: { ...req.body, updatedAt: Date.now(), isNew: false } })
      .then((data) => {
        if (data && Object.keys(data).length > 0) {
          res.status(res.statusCode).json({
            success: true,
            data: { ...data['_doc'], ...req.body },
          });
        } else {
          res.status(res.statusCode).json({ success: false, message: 'emptyUser' });
        }
      })
      .catch((err) => {
        res.status(res.statusCode).json({ success: false, message: err.toString().replace('Error: ', '') });
      });
  }

  public deleteUser(req: Request, res: Response): void {
    const email: string = req.params.email;
    User.findOneAndRemove({ email })
      .then((data) => {
        res.status(res.statusCode).json({
          success: true,
          data,
        });
      })
      .catch((err) => {
        res.status(res.statusCode).json({
          success: false,
          message: err.toString().replace('Error: ', ''),
        });
      });
  }

  routes() {
    this.router.put('/:email', this.updateUser);
    this.router.delete('/:email', this.deleteUser);
    this.router.post('/register', [
      body('fullName').not().isEmpty().withMessage('fullNameEmpty'),
      body('email').isEmail().withMessage('invalidEmailAdrress'),
      body('password').isLength({ min: 4 }).withMessage('passwordEmpty'),
      body('username').not().isEmpty().withMessage('usernameEmpty'),
    ], this.registerUser);
    this.router.post('/login', [
      body('email').isEmail().withMessage('invalidEmailAdrress'),
      body('password').not().isEmpty().withMessage('passwordEmpty'),
    ], this.loginUser);
    this.router.post('/verify', this.verifyToken);
  }
}

const userController = new UserController();

export default userController.router;
